import os
import string
import time
import logging
from datetime import datetime

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.httpclient
from tornado.options import define, options
import tornado.websocket
import uuid

import pdb


define("port", default=8888, help="run on the given port", type=int)

online = []
count = 0


class MainHandler(tornado.web.RequestHandler):

    def get(self):
        self.user = self.get_argument("name", None)
        self.render("index.html", title="Online number testing", c_time=datetime.now(), user=self.user)

class ChatSocketHandler(tornado.websocket.WebSocketHandler):
    waiters = set()
    cache = []
    cache_size = 200 

    def allow_draft76(self):
        # for iOS 5.0 Safari
        return True

    def open(self):
        pdb.set_trace()
        ChatSocketHandler.waiters.add(self)

    def on_close(self):
        ChatSocketHandler.waiters.remove(self)

    @classmethod
    def update_cache(cls, chat):
        cls.cache.append(chat)
        if len(cls.cache) > cls.cache_size:
            cls.cache = cls.cache[-cls.cache_size:]

    @classmethod
    def send_updates(cls, chat):
        logging.info("sending message to %d waiters", len(cls.waiters))
        for waiter in cls.waiters:
            try:
                waiter.write_message(chat)
            except:
                logging.error("Error sending message", exc_info=True)

    def on_message(self, message):
        logging.info("got message %r", message)
        parsed = tornado.escape.json_decode(message)
        chat = { 
            "id": str(uuid.uuid4()),
            "body": parsed["body"],
            }   
        chat["html"] = self.render_string("message.html", message=chat)

        ChatSocketHandler.update_cache(chat)
        ChatSocketHandler.send_updates(chat)

class LongPollingHandler(tornado.web.RequestHandler):

    @tornado.web.asynchronous
    def get(self):
        global online, count
        self.user = self.get_argument("name", None)
        #FIXME(R): This is a debug trigger, later shall be deleted.
        if self.user == 'stop':
            pdb.set_trace()

        if self.user not in online:
            logging.info("user : " + self.user)
            online.append(self.user)
        http = tornado.httpclient.AsyncHTTPClient()
        appURL = self.request.protocol + "://" + self.request.host
        http.fetch(appURL + "/internal-polling", self._on_finish)

    '''push to the client'''
    def _on_finish(self, response):
        if self.request.connection.stream.closed():
            return
        self.write("welcome %s, current online number(s) %s" % (self.user, response.body))
        self.finish()

    def on_connection_close(self):
        raise NotImplementedError()

class InternalPollingHandler(tornado.web.RequestHandler):

    '''
    The internal polling for the new online member which will be counted into
    the global online list, and then asynchronously push the latest data to the connected client,keep in a long-polling status.
    '''
    def get(self):
        global online, count
        logging.info('count: %s | online: %s' % (str(count), str(len(online))))
        if count != len(online):
            count += 1
            self.get()
        else:
            self.write(str(count))

def main():
    tornado.options.parse_command_line()

    settings = {
        "static_path": os.path.join(os.path.dirname(__file__), "static"),
    }

    application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/long-polling", LongPollingHandler),
        (r"/internal-polling", InternalPollingHandler),
        (r'/websocket', ChatSocketHandler),
        ], **settings
    )
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()


