'''
    This is a websocket server based on tornado framework,
    mainly handles real-time messages.
    Author: Lin-Yao Li
    Date  : 06/08/2012
'''

import tornado.ioloop
import tornado.httpserver
import tornado.web
from tornado import websocket
import logging
import tornado.httpclient
import pdb
import time
from datetime import datetime
import os
from tornado.options import define, options

GLOBALS = {
    'sockets' : []
}

define('port', default=8888, help='run on the given port', type=int)

online = []
count = 0

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.user = self.get_argument('name', None)
        self.render('index.html', title='websocket test', c_time=datetime.now(), user=self.user)

class ClientSocket(websocket.WebSocketHandler):
    def open(self):
        GLOBALS['sockets'].append(self) 
        print 'WebSocket opened'

    def on_close(self):
        print "WebSocket closed"
        GLOBALS['sockets'].remove(self)

class Announcer(tornado.web.RequestHandler):
    '''
        Get messages from requests.
    '''
    @tornado.web.asynchronous
    def get(self, *args, **kwargs):
        #NOTE(R): This request should contain at least username and msg context.
        try:
            msg = 'HELLO'
           # msg = self.get_argument('msg')
            user = self.get_argument('user')
            self.msg = msg
            self.user = user
        except Exception, e:
            print e
            raise NotImplementedError()
        logging.info("User %s Connected, message data is >>> %s <<<" % (user, msg))
        for socket in GLOBALS['sockets']:
            socket.write_message(msg)
        http = tornado.httpclient.AsyncHTTPClient()
        appURL = self.request.protocol + '://' + self.request.host
        http.fetch(appURL+'/internal-polling', self.on_close)
    
    def on_close(self, response):
        '''
            push back to client.
        '''
        if self.request.connection.stream.closed():
            return
        self.write('Hey, user %s said %s' % (self.user, self.msg))
        self.finish()

class LongPollingHandler(tornado.web.RequestHandler):

    @tornado.web.asynchronous
    def get(self):
        global online, count
        self.user = self.get_argument("name", None)
        if self.user not in online:
            logging.info("user : " + self.user)
            online.append(self.user)
        http = tornado.httpclient.AsyncHTTPClient()
        appURL = self.request.protocol + "://" + self.request.host
        http.fetch(appURL + "/internal-polling", self._on_finish)

    '''push to the client'''
    def _on_finish(self, response):
        if self.request.connection.stream.closed():
            return
        self.write("welcome %s, current online number(s) %s" % (self.user, response.body))
        self.finish()

class InternalPollingHandler(tornado.web.RequestHandler):
    
    def get(self):
        global online, count
        logging.info('user count: %s, online: %s' % (str(count), str(len(online))))
        if count != len(online):
            count += 1
            self.get()
        else:
            self.write(str(count))

def main():

    tornado.options.parse_command_line()
    logging.basicConfig(level=logging.DEBUG, format="[%(asctime)s] %(message)s")
    settings = {
        'static_path' : os.path.join(os.path.dirname(__file__), 'static'),
    }
    application = tornado.web.Application([
        (r'/', MainHandler),
        (r'/socket', ClientSocket),
        (r'/push', LongPollingHandler) ,
        (r'/internal-polling', InternalPollingHandler),
    ], **settings)
    #NOTE(R): listen at http://localhost:8888/push?data=...
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == '__main__':
    main()





